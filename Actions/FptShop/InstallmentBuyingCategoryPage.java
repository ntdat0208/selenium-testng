package FptShop;

import org.openqa.selenium.By;

import selenium.QTiumAutomation;
import selenium.QTiumElementDefinition;

public class InstallmentBuyingCategoryPage {
	public static synchronized InstallmentBuyingCategoryPage getInstallmentBuyingCategoryPage() {
		if (instance == null)
			instance = new InstallmentBuyingCategoryPage();
		return instance;
	}

	private static InstallmentBuyingCategoryPage instance = null;

	public void removeMainNav() throws Exception {
		System.out.println("> removing MainNav...");
		QTiumAutomation.executeJavaScript("arguments[0].remove()", QTiumElementDefinition.Id("mainNav"));
		System.out.println("> removed!");
	}

	public void selectItem() throws Exception {
		// removeMainNav();
		System.out.println("selecting an item...");
		QTiumAutomation.sleep(5);
		int tab = QTiumAutomation.randomIntNumber(2); // random 1-3
		System.out.println("> selecting tab: " + tab);
		QTiumAutomation.scrollPageToElementAndClick(
				By.xpath(String.format(InstallmentBuyingCategoryPageEntity.ITEM_TAB, tab)));

		QTiumAutomation.sleep(5);
		int item = QTiumAutomation.randomIntNumber(11); // random 1-12
		System.out.println("> selecting item: " + item);
		QTiumAutomation
				.scrollPageToElementAndClick(By.xpath(String.format(InstallmentBuyingCategoryPageEntity.ITEM, item)));
//		QTiumAutomation.switchTab();
	}
}
