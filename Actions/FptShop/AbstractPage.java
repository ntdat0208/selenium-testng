package FptShop;

import java.util.Arrays;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.By;

import selenium.QTiumAssert;
import selenium.QTiumAutomation;
import testng.QTiumTestListener;
import Common.Constants;

public class AbstractPage {

	public static synchronized AbstractPage getAbstractPage() {
		if (instance == null)
			instance = new AbstractPage();
		return instance;
	}

	private static AbstractPage instance = null;

	private static final Log logger = LogFactory.getLog(QTiumTestListener.class);

	// close current page
	public void closePage() {
		QTiumAutomation.close();
	}

	//check page alive
	public void checkPageAlive() throws Exception {

		System.err.println("checking PageAlive...");
		logger.info("checking PageAlive...");
		QTiumAutomation.sleep(5);

		boolean isEachCateStillAlive = true;
		int cateSize = QTiumAutomation.getSize(XiaomiPageEntity.CATE_SIZE);
		System.err.println("cateSize: " + cateSize);
		for (int i = 1; i <= cateSize; i++) {
			QTiumAutomation.click(XiaomiPageEntity.LOADMORE_BUTTON);
			By cateElement = By.xpath(String.format(XiaomiPageEntity.CATE_LINK, i));
			QTiumAutomation.click(cateElement);
			if (QTiumAutomation.getSize(XiaomiPageEntity.XIAOMI_SIZE) > 0) {
				System.err.println("checking cateElement: " + cateElement + ": PASS!!!");
				logger.info("checking cateElement: " + cateElement + ": PASS!!!");
			} else {
				isEachCateStillAlive = false;
				System.err.println("checking cateElement: " + cateElement + ": FALSE!!!");
				logger.info("checking cateElement: " + cateElement + ": FALSE!!!");
				break;
			}
		}

		QTiumAssert.verifyTrue(isEachCateStillAlive);

		System.err.println("checkPageAlive Done!!!!!!");
		logger.info("checkPageAlive Done!!!!!!");
	}

	// advanced search for an item at HomePage
	public void advancedSearchItem() throws Exception {
		System.out.println("advanced Search Item at HomePage: ");
		QTiumAutomation.enter(AbstractPageEntity.SEARCH_TEXTBOX, Constants.FPTSHOP_ITEM_NAME);
		QTiumAutomation.click(AbstractPageEntity.SEARCH_BUTTON);
	}

	// suggestion search for an item at HomePage
	public void suggestionSearchItem() throws Exception {
		System.out.println("suggestion Search Item at HomePage: ");
		logger.info("suggestion Search Item at HomePage: ");
		QTiumAutomation.enter(AbstractPageEntity.SEARCH_TEXTBOX, Constants.FPTSHOP_ITEM_NAME);
		QTiumAutomation.click(AbstractPageEntity.SEARCH_SUGGESTION_ITEM);
	}

	// hover mouse over a category at HomePage
	public void hoverCategory(int category) throws Exception {
		By element = By.xpath(String.format(AbstractPageEntity.CATEGORY, category));
		System.out.println("> hover Category: " + element);
		QTiumAutomation.moveMouseToElement(element);
	}

	// hover mouse over a category at HomePage
	public void hoverCategory(String category) throws Exception {
		By element = By.xpath(String.format(AbstractPageEntity.CATEGORY, category));
		System.out.println("> hover Category: " + element);
		QTiumAutomation.moveMouseToElement(element);
	}

	// remove cart pop-up
	public void removeCartPopup() throws Exception {
		By element = By.xpath("//div[@class='ins-item-left-container']");
		if (QTiumAutomation.isElementDisplayed(element)) {
			System.out.println("> removing 'ins-item-left-container'");
			QTiumAutomation.executeJavaScript("arguments[0].remove();", element);
			System.out.println("> removed!");
		}
	}

	// select a installment buying category at HomePage
	public void selectInstallmentBuyingCategory() throws Exception {
		System.out.println("select installment buying category...");
		removeCartPopup();
		QTiumAutomation.click(AbstractPageEntity.INSTALLMENT_BUYING);
	}

	// select a category at HomePage
	public void selectRandomCategory() throws Exception {
		int category = QTiumAutomation.randomNumberFromList(Arrays.asList(1, 2));
		By element = By.xpath(String.format(AbstractPageEntity.CATEGORY, category));
//		By element = By.xpath(String.format(AbstractPageEntity.CATEGORY, 1));
		System.out.println("selecting category..." + element);
		QTiumAutomation.clickJS(element);
	}

	// select a category at HomePage
	public void selectCategory() throws Exception {
//			int category = QTiumAutomation.randomNumberFromList(Arrays.asList(1, 2));
//			By element = By.xpath(String.format(AbstractPageEntity.CATEGORY, category));
		By element = By.xpath(String.format(AbstractPageEntity.CATEGORY, 1));
		System.out.println("selecting category..." + element);
		QTiumAutomation.clickJS(element);
	}

	// select a category at HomePage
	public void selectCategory(int category) throws Exception {
//			int category = QTiumAutomation.randomNumberFromList(Arrays.asList(1, 2));
//			By element = By.xpath(String.format(AbstractPageEntity.CATEGORY, category));
		By element = By.xpath(String.format(AbstractPageEntity.CATEGORY, 1));
		System.out.println("selecting category..." + element);
		QTiumAutomation.clickJS(element);
	}

	// select a category at HomePage by cateName
	public void selectCategory(String category) throws Exception {
		By element = By.xpath(String.format(AbstractPageEntity.CATEGORY_BY_STRING, category));
		System.out.println("selecting category..." + category + ": " + element);
		QTiumAutomation.clickJS(element);
	}

	// select a brand of a category
	public void selectBrand() throws Exception {
		int category = QTiumAutomation.randomNumberFromList(Arrays.asList(1, 2, 3, 4));
		int brand1 = QTiumAutomation.randomNumberFromList(Arrays.asList(1, 2));
		int brand2 = QTiumAutomation.randomNumberFromList(Arrays.asList(1, 2, 3, 4));
		System.out.println("selecting brand...");
		this.hoverCategory(category);
		QTiumAutomation.clickRandomItem(AbstractPageEntity.BRAND, category, brand1, brand2);
	}

	// select random best buy item
	public void selectBestBuyItem() throws Exception {
		int category = QTiumAutomation.randomNumberFromList(Arrays.asList(1, 3, 4));
		int item = QTiumAutomation.randomNumberFromList(Arrays.asList(1, 2));
		By element = By.xpath(String.format(AbstractPageEntity.BEST_BUY_ITEM, category, item));
		System.out.println("selecting best buy item...");
		this.hoverCategory(category);
		QTiumAutomation.click(element);
	}

	// select random random ranking page
	public void selectRankingPage() throws Exception {
		int category = QTiumAutomation.randomNumberFromList(Arrays.asList(1, 4));
		By element = By.xpath(String.format(AbstractPageEntity.RANKING_PAGE, category));
		System.out.println("selecting ranking page...");
		this.hoverCategory(category);
		QTiumAutomation.click(element);
	}

	// open NewsPage
	public void openNewsPage() throws Exception {
		QTiumAutomation.click(AbstractPageEntity.NEWS_BUTTON);
	}
}