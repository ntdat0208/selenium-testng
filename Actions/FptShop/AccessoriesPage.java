package FptShop;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.By;

import selenium.QTiumAssert;
import selenium.QTiumAutomation;
import testng.QTiumTestListener;

public class AccessoriesPage {
	public static synchronized AccessoriesPage getAccessoriesPage() {
		if (instance == null)
			instance = new AccessoriesPage();
		return instance;
	}

	private static AccessoriesPage instance = null;

	private static final Log logger = LogFactory.getLog(QTiumTestListener.class);

	public void selectRandomAccessory() throws Exception {

		System.err.println("selecting a RandomAccessory...");
		logger.info("selecting a RandomAccessory...");
		QTiumAutomation.sleep(5);

		int randomCate = QTiumAutomation.randomIntNumber(14) + 1;
		By randomCateElement = By.xpath(String.format(AccessoriesPageEntity.CATE_LINK, randomCate));
		QTiumAutomation.click(randomCateElement);

		int randomAccessory = QTiumAutomation.randomIntNumber(QTiumAutomation.getSize(AccessoriesPageEntity.ACCESSORY_SIZE));
		By randomAccessoryElement = By.xpath(String.format(AccessoriesPageEntity.ACCESSORY_LINK, randomAccessory));
		QTiumAutomation.scrollPageToElementAndClick(randomAccessoryElement);

		boolean isItemNameDisplay;

		if (PageFactory.ItemDetailPage().isItemNameDisplay())
			isItemNameDisplay = true;
		else
			isItemNameDisplay = false;

		QTiumAssert.verifyTrue(isItemNameDisplay);

		System.err.println("selectRandomAccessory Done!!!!!!");
		logger.info("selectRandomAccessory Done!!!!!!");
	}

	public void checkEachCateStillAlive() throws Exception {

		System.err.println("checking EachCateStillAlive...");
		logger.info("checking EachCateStillAlive...");
		QTiumAutomation.sleep(5);

		boolean isEachCateStillAlive = true;
		int cateSize = QTiumAutomation.getSize(AccessoriesPageEntity.CATE_SIZE);

		for (int i = 1; i <= cateSize; i++) {
			By cateElement = By.xpath(String.format(AccessoriesPageEntity.CATE_LINK, i));
			QTiumAutomation.click(cateElement);
			if (QTiumAutomation.getSize(AccessoriesPageEntity.ACCESSORY_SIZE) > 0) {
				System.err.println("checking cateElement: " + cateElement + ": PASS!!!");
				logger.info("checking cateElement: " + cateElement + ": PASS!!!");
			} else {
				isEachCateStillAlive = false;
				System.err.println("checking cateElement: " + cateElement + ": FALSE!!!");
				logger.info("checking cateElement: " + cateElement + ": FALSE!!!");
				break;
			}
		}

		QTiumAssert.verifyTrue(isEachCateStillAlive);

		System.err.println("selectRandomAccessory Done!!!!!!");
		logger.info("selectRandomAccessory Done!!!!!!");
	}
}
