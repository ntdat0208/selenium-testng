package FptShop;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.UnhandledAlertException;

import selenium.QTiumAutomation;
import testng.QTiumTestListener;
import Common.Constants;

public class CartPage {
	public static synchronized CartPage getCartPage() {
		if (instance == null)
			instance = new CartPage();
		return instance;
	}

	private static CartPage instance = null;
	
	private static final Log logger = LogFactory.getLog(QTiumTestListener.class);

	// select selectPlaceToReceive
	public void selectPlaceToReceive() throws Exception {
		QTiumAutomation.sleep(5);
		int receive = QTiumAutomation.randomIntNumber(2);
		// int receive = 1;
		if (receive == 1) {
			// receive at shop
			System.out.println("receiveAtShop...");
			logger.info("receiveAtShop...");
			receiveAtShop();
		} else if (receive == 2) {
			System.out.println("receiveAtHome...");
			logger.info("receiveAtHome...");
			// receive at home
			receiveAtHome();
		}
		// else {
		// // skip
		// System.out.println("skipping selectShipping...");
		// QTiumAutomation.click(CartPageEntity.SHIP_SKIP_BUTTON);
		// }
	}

	// select shop to receive at
	public void receiveAtShop() throws Exception {
		try {
			do {
				QTiumAutomation.click(CartPageEntity.RECEIVE_AT_SHOP_TAB);
				QTiumAutomation.selectByIndex(CartPageEntity.SHOP_CITY_DROPDOWN);
				QTiumAutomation.sleep(2);
				// QTiumAutomation.selectByIndex(CartPageEntity.SHOP_DICSTRICT_DROPDOWN);
				// QTiumAutomation.sleep(5);
				// QTiumAutomation.click(CartPageEntity.SHOP_TO_RECEIVE_RADIO);
				QTiumAutomation.click(CartPageEntity.SHIP_CONTINUE_BUTTON);
			} while (!QTiumAutomation.waitForElementDisappear(CartPageEntity.SHIP_CONTINUE_BUTTON));
		} catch (UnhandledAlertException e) {
			System.err.println("UnhandledAlertException!!!");
			QTiumAutomation.dismissAlert();
			receiveAtShop();
		}
	}

	// fill address to receive at
	public void receiveAtHome() throws Exception {
		int i = 0;
		do {
			QTiumAutomation.click(CartPageEntity.RECEIVE_AT_HOME_TAB);

			do {
				QTiumAutomation.selectByIndex(CartPageEntity.HOME_CITY_DROPDOWN);
				QTiumAutomation.sleep(2);
			} while (!QTiumAutomation.checkSelectOptionSize(CartPageEntity.HOME_DICSTRICT_DROPDOWN));

			QTiumAutomation.selectByIndex(CartPageEntity.HOME_DICSTRICT_DROPDOWN);

			QTiumAutomation.enter(CartPageEntity.HOME_ADDRESS_TEXTBOX, "ecom test");
			QTiumAutomation.click(CartPageEntity.SHIP_CONTINUE_BUTTON);
			i++;
		} while (QTiumAutomation.isElementDisplayed(CartPageEntity.SHIPPING_ERROR) && i < 10);
	}

	// select payment
	public void selectPayment() throws Exception {
		QTiumAutomation.sleep(5);
		System.out.println("selecting Payment at CartPage...");
		logger.info("selecting Payment at CartPage...");
		int i = 0;
		do {
			QTiumAutomation.click(CartPageEntity.ADD_ORDER_BUTTON);
			i++;
		} while (QTiumAutomation.isElementDisplayed(CartPageEntity.ADD_ORDER_BUTTON) && i < 10);
	}

	// fill client info, select address & select payment
	public void checkOut() throws Exception {
		fillClientInfo();
		selectPlaceToReceive();
		selectPayment();
	}

	// fast order
	public void fastOrder() throws Exception {
		System.out.println("filling client information at CartPage...");
		do {
			QTiumAutomation.enter(CartPageEntity.CLIENT_NAME_TEXTBOX, Constants.CLIENT_NAME);
			QTiumAutomation.enter(CartPageEntity.CLIENT_PHONE_TEXTBOX, Constants.CLIENT_PHONE);
			QTiumAutomation.enter(CartPageEntity.CLIENT_EMAIL_TEXTBOX, Constants.CLIENT_EMAIL);
			QTiumAutomation.click(CartPageEntity.ORDER_FAST_BUTTON);
		} while (QTiumAutomation.isElementDisplayed(CartPageEntity.CLIENT_NAME_ERROR)
				|| QTiumAutomation.isElementDisplayed(CartPageEntity.CLIENT_PHONE_ERROR)
				|| QTiumAutomation.isElementClickable(CartPageEntity.ORDER_FAST_BUTTON));
	}

	public void fillClientInfo() throws Exception {
		QTiumAutomation.sleep(5);
		System.out.println("filling client information at CartPage...");
		logger.info("filling client information at CartPage...");
		do {
			QTiumAutomation.enter(CartPageEntity.CLIENT_NAME_TEXTBOX, Constants.CLIENT_NAME);
			QTiumAutomation.enter(CartPageEntity.CLIENT_PHONE_TEXTBOX, Constants.CLIENT_PHONE);
			QTiumAutomation.click(CartPageEntity.CLIENT_CONTINUE_BUTTON);
			QTiumAutomation.sleep(5);
		} while (QTiumAutomation.isElementDisplayed(CartPageEntity.CLIENT_NAME_ERROR)
				|| QTiumAutomation.isElementDisplayed(CartPageEntity.CLIENT_PHONE_ERROR)
				|| QTiumAutomation.isElementClickable(CartPageEntity.CLIENT_CONTINUE_BUTTON));
	}
}
