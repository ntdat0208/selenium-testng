package FptShop;

import selenium.QTiumAssert;
import selenium.QTiumAutomation;
import testng.QTiumTestListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import Common.Constants;

public class ItemDetailPage {
	public static synchronized ItemDetailPage getItemDetailPage() {
		if (instance == null)
			instance = new ItemDetailPage();
		return instance;
	}

	private static ItemDetailPage instance = null;

	private static final Log logger = LogFactory.getLog(QTiumTestListener.class);

	// skip chat pop up
	public void removeChat() throws Exception {
		System.out.println("removing chat iframe...");
		if (QTiumAutomation.waitForElement(ItemDetailPageEntity.CHAT))
			try {
				// ((JavascriptExecutor) QTiumAutomation.getDriver())
				// .executeScript("document.getElementsByClassName('sbzon')[0].remove()");
				QTiumAutomation.executeJavaScript("arguments[0].remove()", ItemDetailPageEntity.CHAT);
				System.err.println(">>> removed!");
			} catch (Exception e) {
				System.err.println("removeChat - Exception: " + e);
			}
	}

	// skip chat pop up
	public void removePromotion() throws Exception {
		System.out.println("removing promotion pop up...");
		if (QTiumAutomation.waitForElement(ItemDetailPageEntity.PROMOTION_POPUP))
			try {
				// ((JavascriptExecutor) QTiumAutomation.getDriver())
				// .executeScript("document.getElementsByClassName('sbzon')[0].remove()");
				QTiumAutomation.executeJavaScript("arguments[0].remove()", ItemDetailPageEntity.PROMOTION_POPUP);
				System.err.println(">>> removed!");
			} catch (Exception e) {
				System.err.println("removeChat - Exception: " + e);
			}
	}

	public void contactWhenAvailable() throws Exception {
		System.out.println("contactWhenAvailable...");
		if (QTiumAutomation.isElementDisplayed(ItemDetailPageEntity.OLD_HEADER)) {
			System.out.println(">>> landing!");
			logger.info(">>> landing!");
			QTiumAssert.verifyTrue(false);
		} else {
			QTiumAutomation.scrollPageToElementAndClick(ItemDetailPageEntity.CONTACT_WHEN_AVAILABLE_BUTTON);
			QTiumAutomation.enter(ItemDetailPageEntity.CLIENT_NAME_TEXTBOX, Constants.CLIENT_NAME);
			QTiumAutomation.enter(ItemDetailPageEntity.CLIENT_PHONE_TEXTBOX, Constants.CLIENT_PHONE);
			QTiumAutomation.click(ItemDetailPageEntity.SUBMIT_BUTTON);
			if (QTiumAutomation.isElementClickable(ItemDetailPageEntity.ORDER_SUCCESSFULLLY_BUTTON))
				QTiumAssert.verifyTrue(true);
			else
				QTiumAssert.verifyTrue(false);
		}

		System.err.println("Done!!!");
		logger.info("Done!!!");
	}

	public boolean isItemNameDisplay() throws Exception {
		QTiumAutomation.sleep(5);
		System.out.println("is ItemName Display at ItemDetailPage...");
		logger.info("is ItemName Display at ItemDetailPage...");
		
		if (QTiumAutomation.isElementDisplayed(ItemDetailPageEntity.ITEM_NAME_LABEL)) {
			String itemName = QTiumAutomation.findElement(ItemDetailPageEntity.ITEM_NAME_LABEL).getText();
			System.err.println(">>> item name:" + itemName);
			logger.info(">>> item name:" + itemName);
			return true;
		} else {
			return false;
		}
	}

	public boolean checkItemStatus() throws Exception {
		QTiumAutomation.sleep(5);
		if (QTiumAutomation.isElementDisplayed(ItemDetailPageEntity.OLD_HEADER)) {
			System.out.println(">>> landing!");
			logger.info(">>> landing!");
			return false;
		} else {
			removeChat();
			removePromotion();
			System.out.println("checking item status...");
			logger.info("checking item status...");
			if (QTiumAutomation.isElementDisplayed(ItemDetailPageEntity.CONTACT_WHEN_AVAILABLE_BUTTON)) {
				System.out.println(">>> out of stock!");
				logger.info(">>> out of stock!");
				return false;
			} else {
				System.out.println(">>> in stock!");
				logger.info(">>> in stock!");
				return true;
			}
		}
	}

	public void buyNow() throws Exception {
		QTiumAutomation.sleep(5);
		System.out.println("selecting buyNow at ItemDetailPage...");
		logger.info("selecting buyNow at ItemDetailPage...");
		QTiumAutomation.scrollPageToElementAndClick(ItemDetailPageEntity.BUYNOW_BUTTON);
	}

	public void shopBooking() throws Exception {
		QTiumAutomation.sleep(5);
		System.out.println("selecting shopBooking at ItemDetailPage...");
		QTiumAutomation.scrollPageToElementAndClick(ItemDetailPageEntity.SHOP_BOOKING_BUTTON);
		QTiumAutomation.scrollPageToElementAndClick(ItemDetailPageEntity.SHOP_BOOKING_LINK);
	}

	public void installmentBuying() throws Exception {
		QTiumAutomation.sleep(5);
		System.out.println("selecting installment buying at ItemDetailPage...");
		QTiumAutomation.scrollPageToElementAndClick(ItemDetailPageEntity.INSTALLMENT_BUYING_BUTTON);
	}
}
