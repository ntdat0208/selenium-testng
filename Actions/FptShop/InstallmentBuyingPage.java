package FptShop;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.By;

import selenium.QTiumAssert;
import selenium.QTiumAutomation;
import selenium.QTiumElementDefinition;
import testng.QTiumTestListener;
import Common.Constants;

public class InstallmentBuyingPage {
	public static synchronized InstallmentBuyingPage getInstallmentBuyingPage() {
		if (instance == null)
			instance = new InstallmentBuyingPage();
		return instance;
	}

	private static InstallmentBuyingPage instance = null;

	private static final Log logger = LogFactory.getLog(QTiumTestListener.class);

	public void selectInstallmentBuyingMethod() throws Exception {
		
		logger.info("selecting installment buying method...");

		if (QTiumAutomation.isElementDisplayed(InstallmentBuyingPageEntity.FE_CREDIT_BUTTON)) {
			logger.info("selected FE CREDIT installment buying method");
			QTiumAutomation.scrollPageToElementAndClick(InstallmentBuyingPageEntity.FE_CREDIT_BUTTON);
		} else {
			logger.info("selected HOME CREDIT installment buying method");
			QTiumAutomation.scrollPageToElementAndClick(InstallmentBuyingPageEntity.HOME_CREDIT_BUTTON);
		}
		
		if (QTiumAutomation.isElementDisplayed(InstallmentBuyingPageEntity.CLIENT_FULLNAME_TEXTBOX)) {
			logger.info("filling FE CREDIT installment buying info");
			feInstallmentBuying();
		} else {
			logger.info("filling HOME CREDIT installment buying info");
			homeInstallmentBuying();
		}
	}

	public void feInstallmentBuying() throws Exception {

		logger.info("filling InformationInstallmentBuying...");

		int sex = QTiumAutomation.randomIntNumber(1);
		By sex_xpath = QTiumElementDefinition.Xpath(String.format(InstallmentBuyingPageEntity.CLIENT_SEX_RADIO, sex));
		QTiumAutomation.scrollPageToElementAndClick(sex_xpath);

		do {
			QTiumAutomation.scrollPageToElementAndEnter(InstallmentBuyingPageEntity.CLIENT_FULLNAME_TEXTBOX,
					Constants.CLIENT_NAME);
			QTiumAutomation.enter(InstallmentBuyingPageEntity.CLIENT_PHONE_NUMBER_TEXTBOX, Constants.CLIENT_PHONE);
			QTiumAutomation.enter(InstallmentBuyingPageEntity.CLIENT_ID_TEXTBOX, Constants.CLIENT_ID);
			QTiumAutomation.enter(InstallmentBuyingPageEntity.CLIENT_BIRTH_DAY_TEXTBOX,
					Integer.toString(QTiumAutomation.randomIntNumber(27)));
			QTiumAutomation.enter(InstallmentBuyingPageEntity.CLIENT_BIRTH_MONTH_TEXTBOX,
					Integer.toString(QTiumAutomation.randomIntNumber(11)));
			QTiumAutomation.enter(InstallmentBuyingPageEntity.CLIENT_BIRTH_YEAR_TEXTBOX,
					Integer.toString(QTiumAutomation.randomIntNumber(50) + 1950));

			do {
				QTiumAutomation.selectByIndex(InstallmentBuyingPageEntity.CLIENT_CITY_DROPDOWN);
				QTiumAutomation.sleep(2);
			} while (!QTiumAutomation.checkSelectOptionSize(InstallmentBuyingPageEntity.CLIENT_DICSTRICT_DROPDOWN));

			QTiumAutomation.selectByIndex(InstallmentBuyingPageEntity.CLIENT_DICSTRICT_DROPDOWN);

			QTiumAutomation.scrollPageToElementAndEnter(InstallmentBuyingPageEntity.CLIENT_ADDRESS_TEXTBOX,
					Constants.CLIENT_ADDRESS);
			QTiumAutomation.enter(InstallmentBuyingPageEntity.CLIENT_GUARANTOR_PHONE_NUMBER_TEXTBOX,
					Constants.CLIENT_GUARANTOR_PHONE);

			do {
				QTiumAutomation.selectByIndex(InstallmentBuyingPageEntity.SHOP_CITY_DROPDOWN);
				QTiumAutomation.sleep(2);
			} while (!QTiumAutomation.checkSelectOptionSize(InstallmentBuyingPageEntity.SHOP_DICSTRICT_DROPDOWN));

			QTiumAutomation.selectByIndex(InstallmentBuyingPageEntity.SHOP_DICSTRICT_DROPDOWN);

			QTiumAutomation.click(InstallmentBuyingPageEntity.ADD_ORDER_BUTTON);

		} while (QTiumAutomation.isElementDisplayed(InstallmentBuyingPageEntity.CLIENT_NAME_ERROR_MESSAGE)
				|| QTiumAutomation.isElementDisplayed(InstallmentBuyingPageEntity.CLIENT_PHONE_NUMBER_ERROR_MESSAGE)
				|| QTiumAutomation.isElementDisplayed(InstallmentBuyingPageEntity.CLIENT_ID_ERROR_MESSAGE)
				|| QTiumAutomation.isElementDisplayed(InstallmentBuyingPageEntity.CLIENT_BIRTH_DAY_ERROR_MESSAGE)
				|| QTiumAutomation.isElementDisplayed(InstallmentBuyingPageEntity.CLIENT_CITY_ERROR_MESSAGE)
				|| QTiumAutomation.isElementDisplayed(InstallmentBuyingPageEntity.CLIENT_DICSTRICT_ERROR_MESSAGE)
				|| QTiumAutomation.isElementDisplayed(InstallmentBuyingPageEntity.CLIENT_ADDRESS_ERROR_MESSAGE)
				|| QTiumAutomation
						.isElementDisplayed(InstallmentBuyingPageEntity.CLIENT_GUARANTOR_PHONE_NUMBER_ERROR_MESSAGE)
				|| QTiumAutomation.isElementDisplayed(InstallmentBuyingPageEntity.ADD_ORDER_BUTTON));

	}

	public void homeInstallmentBuying() throws Exception {

		logger.info("filling InformationInstallmentBuying...");

		int sex = QTiumAutomation.randomIntNumber(1);
		By sex_xpath = QTiumElementDefinition.Xpath(String.format(InstallmentBuyingPageEntity.CLIENT_SEX_RADIO, sex));
		QTiumAutomation.scrollPageToElementAndClick(sex_xpath);

		do {
			QTiumAutomation.scrollPageToElementAndEnter(InstallmentBuyingPageEntity.CLIENT_FIRSTNAME_TEXTBOX,
					Constants.CLIENT_FIRSTNAME);
			QTiumAutomation.scrollPageToElementAndEnter(InstallmentBuyingPageEntity.CLIENT_MIDNAME_TEXTBOX,
					Constants.CLIENT_MIDNAME);
			QTiumAutomation.scrollPageToElementAndEnter(InstallmentBuyingPageEntity.CLIENT_LASTNAME_TEXTBOX,
					Constants.CLIENT_LASTNAME);
			QTiumAutomation.enter(InstallmentBuyingPageEntity.CLIENT_ID_TEXTBOX, Constants.CLIENT_ID);
			QTiumAutomation.enter(InstallmentBuyingPageEntity.CLIENT_BIRTH_DAY_TEXTBOX,
					Integer.toString(QTiumAutomation.randomIntNumber(27)));
			QTiumAutomation.enter(InstallmentBuyingPageEntity.CLIENT_BIRTH_MONTH_TEXTBOX,
					Integer.toString(QTiumAutomation.randomIntNumber(11)));
			QTiumAutomation.enter(InstallmentBuyingPageEntity.CLIENT_BIRTH_YEAR_TEXTBOX,
					Integer.toString(QTiumAutomation.randomIntNumber(50) + 1950));
			QTiumAutomation.enter(InstallmentBuyingPageEntity.CLIENT_PHONE_NUMBER_TEXTBOX, Constants.CLIENT_PHONE);

			QTiumAutomation.selectByIndex(InstallmentBuyingPageEntity.CLIENT_DICSTRICT_DROPDOWN);

			do {
				QTiumAutomation.selectByIndex(InstallmentBuyingPageEntity.SHOP_CITY_DROPDOWN);
				QTiumAutomation.sleep(2);
			} while (!QTiumAutomation.checkSelectOptionSize(InstallmentBuyingPageEntity.SHOP_DICSTRICT_DROPDOWN));

			QTiumAutomation.selectByIndex(InstallmentBuyingPageEntity.SHOP_DICSTRICT_DROPDOWN);

			QTiumAutomation.click(InstallmentBuyingPageEntity.ADD_ORDER_BUTTON);

		} while (QTiumAutomation.isElementDisplayed(InstallmentBuyingPageEntity.CLIENT_NAME_ERROR_MESSAGE)
				|| QTiumAutomation.isElementDisplayed(InstallmentBuyingPageEntity.CLIENT_PHONE_NUMBER_ERROR_MESSAGE)
				|| QTiumAutomation.isElementDisplayed(InstallmentBuyingPageEntity.CLIENT_ID_ERROR_MESSAGE)
				|| QTiumAutomation.isElementDisplayed(InstallmentBuyingPageEntity.CLIENT_BIRTH_DAY_ERROR_MESSAGE)
				|| QTiumAutomation.isElementDisplayed(InstallmentBuyingPageEntity.CLIENT_CITY_ERROR_MESSAGE)
				|| QTiumAutomation.isElementDisplayed(InstallmentBuyingPageEntity.CLIENT_DICSTRICT_ERROR_MESSAGE)
				|| QTiumAutomation.isElementDisplayed(InstallmentBuyingPageEntity.CLIENT_ADDRESS_ERROR_MESSAGE)
				|| QTiumAutomation
						.isElementDisplayed(InstallmentBuyingPageEntity.CLIENT_GUARANTOR_PHONE_NUMBER_ERROR_MESSAGE)
				|| QTiumAutomation.isElementDisplayed(InstallmentBuyingPageEntity.ADD_ORDER_BUTTON));

	}

	public void checkInstallmentBuyingResult() throws Exception {
		
		logger.info("checking InstallmentBuyingResult...");
		
		QTiumAutomation.sleep(5);
		boolean isElementDisplayed = QTiumAutomation
				.waitForElement(InstallmentBuyingPageEntity.SUCCESSFULLY_ORDER_LABEL);
		QTiumAssert.verifyTrue(isElementDisplayed);
		System.err.println("Done!!!");
	}
}
