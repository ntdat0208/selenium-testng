package Common;

import selenium.QTiumSetting;

public class Common {

	public static void config() {
		// Configure browser to launch
		System.err.println("set object wait: " + Constants.ObjectWait + "s");
		QTiumSetting.setObjecWait(Constants.ObjectWait);
		System.err.println("set page wait: " + Constants.PageWait + "s");
		QTiumSetting.setPageWait(Constants.PageWait);
	}
}
