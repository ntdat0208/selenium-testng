package Common;

public class Constants {
	
	public static final int ObjectWait = 10;
	public static final int PageWait = 20;
	
	// ---------------------------------FPT Shop Data----------------------------------//
	public static final String FPTSHOP_URL = "https://fptshop.com.vn";
	public static final String LONGCHAU_URL = "https://nhathuoclongchau.com";
	public static final String FPTSHOP_XIAOMI_PAGE = "https://fptshop.com.vn/xiaomi";
	
	// ---------------------------------Item Info----------------------------------//
	public static final String FPTSHOP_ITEM_NAME = "iPhone 8 64GB";
	public static final String LONGCHAU_ITEM_NAME = "Nanocurcumin";
	
	public static final String FPTSHOP_PHUKIEN_CATE = "PHỤ KIỆN";	
	
	// ---------------------------------Client Info----------------------------------//
	public static final String CLIENT_NAME = "frt ecom test";
	public static final String CLIENT_FIRSTNAME = "frt";
	public static final String CLIENT_MIDNAME = "ecom";
	public static final String CLIENT_LASTNAME = "test";
	public static final String CLIENT_PHONE = "0909090909";
	public static final String CLIENT_EMAIL = "frt-ecom-test@fpt.com.vn";
	
	//----------------------------------Installment Buying Info-------------------------//	
	public static final String CLIENT_ID = "111111111";
	public static final String CLIENT_BIRTH_DAY = "1";
	public static final String CLIENT_BIRTH_MONTH = "1";
	public static final String CLIENT_BIRTH_YEAR = "1991";
	public static final String CLIENT_ADDRESS = "ecom test";
	public static final String CLIENT_GUARANTOR_PHONE = "0888080808";
	
}
