package FptShop;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import Common.Common;
import Common.Constants;
import selenium.QTiumAutomation;

public class PhuKienTests {

	@BeforeSuite
	public void setUp() throws Exception {
		Common.config();
//		BasicConfigurator.configure();
	}

	@AfterMethod
	public void tearDown() {
		try {
//			QTiumAutomation.sleep(300);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PageFactory.AbstractPage().closePage();
	}

	@BeforeMethod
	public void prepair() throws Exception {
		// Open HomePage
		// PageFactory.HomePage().open();
	}

	@AfterSuite
	public void finishSuite() {
//		QTiumAutomation.sendEmail(Constants.CLIENT_EMAIL, Constants.CLIENT_EMAIL, Constants.CLIENT_EMAIL);
	}

	@Test(description = "TC 001a: Verify Order An Item By Search Suggestion Successfully", invocationCount = 1000)
	public void TC002a_Detail_Display_Successfully() throws Exception {
		// Open HomePage
		PageFactory.HomePage().open();
		// Search for item at HomePage
		PageFactory.HomePage().selectCategory(Constants.FPTSHOP_PHUKIEN_CATE);
		// Check if item available at ItemDetailPage
		PageFactory.AccessoriesPage().selectRandomAccessory();
	}

	//@Test(description = "TC 001a: Verify Order An Item By Search Suggestion Successfully")
	public void TC001a_Cate_Display_Successfully() throws Exception {
		// Open HomePage
		PageFactory.HomePage().open();
		// Search for item at HomePage
		PageFactory.HomePage().selectCategory(Constants.FPTSHOP_PHUKIEN_CATE);
		// Check if item available at ItemDetailPage
		PageFactory.AccessoriesPage().checkEachCateStillAlive();
	}
}
