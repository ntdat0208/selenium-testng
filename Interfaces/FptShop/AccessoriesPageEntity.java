package FptShop;

import org.openqa.selenium.By;

import selenium.QTiumElementDefinition;

public class AccessoriesPageEntity {

	public static By CATE_SIZE = QTiumElementDefinition.Xpath("//div[@class='pk-menu'][1]/ul/li");
	public static String CATE_LINK = "//div[@class='pk-menu'][1]/ul/li[%s]/a";
	
	public static By ACCESSORY_SIZE = QTiumElementDefinition.Xpath("//div[contains(@class,'pk-splist')]//div[@class='pk-spit']");
	public static String ACCESSORY_LINK = "//div[%s]/div[@class='pk-spit']/a";
	
}
